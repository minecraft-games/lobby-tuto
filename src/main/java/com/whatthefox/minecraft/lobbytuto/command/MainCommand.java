package com.whatthefox.minecraft.lobbytuto.command;

import com.whatthefox.minecraft.lobbytuto.LobbyTutoPlugin;
import com.whatthefox.minecraft.lobbytuto.service.LobbyService;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class MainCommand implements CommandExecutor {

  @Override
  public boolean onCommand(@NotNull final CommandSender commandSender, @NotNull final Command cmd, @NotNull final String details,
      final @NotNull String[] args) {
    if (!(commandSender instanceof Player)) {
      commandSender.sendMessage(ChatColor.RED + "Nop! You must be a player to use this command.");
    } else if (!commandSender.isOp()) {
      commandSender.sendMessage(ChatColor.RED + "Nop! You are not allowed to use this command.");
    } else if (args.length == 0) {
      sendUsage(commandSender);
    } else if ("debug".equalsIgnoreCase(args[0])) {
      LobbyService.getInstance().switchDebugMode();
      commandSender.sendMessage(ChatColor.GREEN + "Debug mode switched to " + LobbyService.getInstance().isDebugMode());
    } else if ("lobby".equalsIgnoreCase(args[0])) {
      if (args.length < 3) {
        sendUsage(commandSender);
      } else {
        final String tutoName = args[2].toLowerCase();
        final String prefix = "lobby." + tutoName + ".";
        if ("set".equalsIgnoreCase(args[1])) {
          final Player player = (Player) commandSender;
          LobbyTutoPlugin.getInstance().getConfig().set(prefix + "world", player.getWorld().getName());
          LobbyTutoPlugin.getInstance().getConfig().set(prefix + "x", player.getLocation().getX());
          LobbyTutoPlugin.getInstance().getConfig().set(prefix + "y", player.getLocation().getY());
          LobbyTutoPlugin.getInstance().getConfig().set(prefix + "z", player.getLocation().getZ());
          LobbyTutoPlugin.getInstance().getConfig().set(prefix + "yaw", player.getLocation().getYaw());
          LobbyTutoPlugin.getInstance().getConfig().set(prefix + "pitch", player.getLocation().getPitch());
          LobbyTutoPlugin.getInstance().saveConfig();
          LobbyService.getInstance().registerLobby(tutoName, player.getLocation());
          commandSender.sendMessage(ChatColor.GREEN + "Lobby " + tutoName + " set!");
        } else if ("del".equalsIgnoreCase(args[1])) {
          LobbyTutoPlugin.getInstance().getConfig().set(prefix, null);
          LobbyTutoPlugin.getInstance().saveConfig();
          if (LobbyService.getInstance().unregisterLobby(args[2].toLowerCase())) {
            commandSender.sendMessage(ChatColor.GREEN + "Lobby " + tutoName + " deleted!");
          } else {
            commandSender.sendMessage(ChatColor.RED + "Lobby " + tutoName + " not found!");
          }
        } else {
          sendUsage(commandSender);
        }
      }
    } else {
      sendUsage(commandSender);
    }
    return false;
  }

  private void sendUsage(final CommandSender commandSender) {
    commandSender.sendMessage("Baka! Usage: /lobbytuto [lobby|debug] [set|del] <name>");
  }
}
