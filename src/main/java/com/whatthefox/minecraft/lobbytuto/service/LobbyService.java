package com.whatthefox.minecraft.lobbytuto.service;

import com.whatthefox.minecraft.lobbytuto.LobbyTutoPlugin;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class LobbyService {

  private final Map<String, Location> lobbies = new HashMap<>();
  private final Map<String, Player> usedLobbies = new HashMap<>();
  private static LobbyService instance;
  private boolean debug = false;


  public static LobbyService getInstance() {
    if (instance == null) {
      instance = new LobbyService();
    }
    return instance;
  }

  public void load() {
    LobbyTutoPlugin.getInstance().getLogger().info("Loading lobbies...");
    final FileConfiguration config = LobbyTutoPlugin.getInstance().getConfig();
    if (!config.isConfigurationSection("lobby")) {
      LobbyTutoPlugin.getInstance().getLogger().info("No lobby in config file, loading done !");
      return;
    }
    for (final String name : config.getConfigurationSection("lobby").getKeys(false)) {
      final String path = "lobby." + name;
      try {
        registerLobby(name, buildLocation(path));
      } catch (Exception ex) {
        LobbyTutoPlugin.getInstance().getLogger().warning("Error while loading lobby " + name + ": " + ex.getMessage());
      }
    }
    LobbyTutoPlugin.getInstance().getLogger().info("Lobbies loaded!");
  }

  private Location buildLocation(final String path) {
    final FileConfiguration config = LobbyTutoPlugin.getInstance().getConfig();
    final String worldName = config.getString(path + ".world");
    if (worldName == null) {
      return null;
    }
    final World world = Bukkit.getWorld(worldName);
    final double x = config.getDouble(path + ".x");
    final double y = config.getDouble(path + ".y");
    final double z = config.getDouble(path + ".z");
    final int yaw = config.getInt(path + ".yaw");
    final int pitch = config.getInt(path + ".pitch");
    return new Location(world, x, y, z, yaw, pitch);
  }

  public void registerLobby(final String name, final Location location) {
    lobbies.put(name, location);
    usedLobbies.put(name, null);
  }

  public boolean unregisterLobby(final String name) {
    usedLobbies.remove(name);
    return Objects.nonNull(lobbies.remove(name));
  }

  public Location borrowLobby(final Player player) {
    for (final Map.Entry<String, Player> entry : usedLobbies.entrySet()) {
      if (entry.getValue() == null) {
        usedLobbies.put(entry.getKey(), player);
        return lobbies.get(entry.getKey());
      }
    }
    return null;
  }

  public void returnLobby(final Player player) {
    for (final Map.Entry<String, Player> entry : usedLobbies.entrySet()) {
      if (entry.getValue() == player) {
        usedLobbies.put(entry.getKey(), null);
        return;
      }
    }
  }

  public void switchDebugMode() {
    debug = !debug;
  }

  public boolean isDebugMode() {
    return debug;
  }
}
