package com.whatthefox.minecraft.lobbytuto;

import com.whatthefox.minecraft.lobbytuto.command.MainCommand;
import com.whatthefox.minecraft.lobbytuto.event.PlayerEvent;
import com.whatthefox.minecraft.lobbytuto.service.LobbyService;
import org.bukkit.plugin.java.JavaPlugin;

public class LobbyTutoPlugin extends JavaPlugin {

  public static LobbyTutoPlugin getInstance() {
    return getPlugin(LobbyTutoPlugin.class);
  }

  @Override
  public void onEnable() {
    // Load config file
    getConfig().options().copyDefaults(true);
    saveDefaultConfig();

    // Load lobbies
    LobbyService.getInstance().load();

    // Register command
    getCommand("lobbytuto").setExecutor(new MainCommand());

    // Register events
    getServer().getPluginManager().registerEvents(new PlayerEvent(), this);
  }
}
