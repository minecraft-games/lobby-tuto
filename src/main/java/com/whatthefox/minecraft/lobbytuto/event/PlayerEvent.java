package com.whatthefox.minecraft.lobbytuto.event;

import com.whatthefox.minecraft.lobbytuto.service.LobbyService;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerEvent implements Listener {

  @EventHandler
  public void onPlayerJoin(final PlayerJoinEvent event) {
    if (!event.getPlayer().hasPlayedBefore() || LobbyService.getInstance().isDebugMode()) {
      final Location location = LobbyService.getInstance().borrowLobby(event.getPlayer());
      if (location == null) {
        event.getPlayer().sendMessage(ChatColor.RED + "No lobby available, please contact an administrator");
      } else {
        event.getPlayer().teleport(location);
      }
    }
  }

  @EventHandler
  public void onPlayerQuit(final PlayerQuitEvent event) {
    LobbyService.getInstance().returnLobby(event.getPlayer());
  }

  @EventHandler
  public void onPlayerQuit(final PlayerChangedWorldEvent event) {
    LobbyService.getInstance().returnLobby(event.getPlayer());
  }
}
